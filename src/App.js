import './App.css';
import { useState, useEffect } from 'react'
import { Wheel } from 'react-custom-roulette'

function App() {
  let data = [
    { option: 'Makanan' },
    { option: 'Boneka' },
    { option: 'Minuman' },
  ]

  const [tampungan, settampungan] = useState(data)

  const [nama, setNama] = useState("")
  const [tLahir, settLahir] = useState("")
  const [hadiah, setHadiah] = useState("")
  const [sepin, setSepin] = useState(false)

  const handleChangeHadiah = (event) =>{
    setHadiah(event.target.value)
  }

  const handleChangeNama = (event) => {
    setNama(event.target.value)
  }

  const handleChangeTlahir = (event) => {
    settLahir(event.target.value)
  }

  const [ultah, setUltah] = useState(false)
  
  const handleSubmit = (event) => {
    event.preventDefault()
    var today = new Date()
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate()
    if (date === tLahir) {
      setUltah(true)
    } else {
      setUltah(false)
    }
  }

  const [mustSpin, setMustSpin] = useState(false);
  const [prizeNumber, setPrizeNumber] = useState("");
  const [tampil, setTampil] = useState(0)

  const handleSpinClick = () => {
    let newPrizeNumber = Math.floor(Math.random() * tampungan.length)
    setPrizeNumber(newPrizeNumber)
    setMustSpin(true)
  }

  useEffect(() => {
    const timer = setTimeout(() => {
      setTampil(tampungan[prizeNumber].option)
      setSepin(true)
    }, 9000)
    return () => clearTimeout(timer)
  }, [prizeNumber])

  const handleT = () =>{
    settampungan([...tampungan, {option: hadiah}])
  }

  return (
    <>
      {
        ultah && (
          <>
            <div class="bg"></div>
            <div class="bg bg2"></div>
            <div class="bg bg3"></div>
          </>
        )
      }
      <div className="App container" id="balloon-container">
        <h1 className="text-center">Website</h1>
        <h2 className="text-center">Happy Birthday</h2>
        <form onSubmit={handleSubmit} className="pembungkus-form">
          <div>
            <div>
              <label>Nama</label>
            </div>
            <input type="text" name="nama" onChange={handleChangeNama} value={nama} />
          </div>
          <div>
            <div>
              <label>Tanggal Lahir</label>
            </div>
            <input type="date" name="tLahir" onChange={handleChangeTlahir} value={tLahir} />
          </div>
          <input type="submit" className="button-5" />
        </form>
      </div>
      {
        ultah ? (
          <>
            <h1 className="text-center">HELLO {nama.toUpperCase()}</h1>
            <h2 className="text-center">WE WISH YOU HAPPY BIRTHDAY ON {tLahir}</h2>
            <div style={{maxWidth:'200px', marginLeft:'30px'}}>
            <Wheel
              style={{display:'flex', justifyContent:'center'}}
              mustStartSpinning={mustSpin}
              prizeNumber={prizeNumber}
              data={tampungan}

              onStopSpinning={() => {
                setMustSpin(false)
              }}
            />
            <button className='button-42' onClick={handleSpinClick}>Let's Spin</button> 
            {
              sepin ? (<h1 className='text-center'>CONGRATZ {nama} YOU GOT {tampil} FROM US</h1>) : (<h1>Belum Spin</h1>)
            }
            </div>
            
            <div style={{background:'#61dafb', width: '400px', margin:'20px auto',  border:'solid 5px hsla(0, 95%, 35%, 1)', padding:'50px'}}>
              <label>Input Hadiah</label>
              <input type="text" value={hadiah} onChange={handleChangeHadiah} />
              <button className='button-84' onClick={handleT}>Tambah</button>
            </div>
            <div className="text-center">
              <div class="balloon"></div>
              <div class="balloon"></div>
              <div class="balloon"></div>
              <div class="balloon"></div>
              <div class="balloon"></div>
            </div>
          </>

        ) : (
          <>
            <h1 className="text-center">Anda Belum Ulang Tahun</h1>
          </>
        )
      }
    </>
  );
}

export default App;
